import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Post(props) {
    const [post, setPost] = useState(null);

    useEffect(() => {
        let id = props.match.params.post_id;
        async function fetchData() {
            await axios.get('https://jsonplaceholder.typicode.com/posts/' + id)
                .then(res => {
                    setPost(res.data);
                    //console.log(res)
                });
        }
        fetchData();
        console.log(props);

    }, []);

    const posts = post ? (
        <div className="post">
            <h4 className="center">
                {post.title}
            </h4>
            <p>{post.body}</p>
        </div>
    ) : (
            <div className="center">Loading post...</div>
        )

    return (
        <div className="container">
            {posts}
        </div>
    )
}

export default Post;