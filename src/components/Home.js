import React, { useState, useEffect } from 'react';
//import axios from 'axios';
import { Link } from 'react-router-dom';
import Pockeball from '../pokeball.png';
import { connect } from 'react-redux';

const Home = (props) => {
    // const [posts, setPosts] = useState([]);
    // useEffect(() => {

    //     async function fetchData() {
    //         await axios.get('https://jsonplaceholder.typicode.com/posts')
    //             .then(res => {
    //                 //console.log(res);
    //                 const slicing = res.data.slice(0, 10);
    //                 setPosts(slicing);
    //             });
    //     }

    //     fetchData();
    //     console.log('Home');

    // }, []);
    console.log(props);
    const { posts } = props;

    const postList = posts.length ? (
        posts.map(post => {
            return (
                <div className="post card" key={post.id} >
                    <img src={Pockeball} alt="A pokeball" />
                    <div className="card-content">
                        <Link to={'/' + post.id}>
                            <span className="card-title red-text">{post.title}</span>
                        </Link>
                        <p>{post.body}</p>
                    </div>
                </div>
            )
        })
    ) : (
            <div className="center">No post yet!</div>
        );

    return (
        <div className="container home">
            <h4 className="center">Home</h4>
            {postList}
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        posts: state.posts
    }
}

export default connect(mapStateToProps)(Home);